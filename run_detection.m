clear all; close all; clc;
%%  Demo MATLAB code 
% Paper: "Fast surface detection in single-photon Lidar waveforms"
% by J. Tachella, Y. Altmann, J.-Y. Tourneret and S. McLaughlin
% published in EUSIPCO 2019, La Coru�a, Spain
% Contact: Julian Tachella, jat3@hw.ac.uk
%
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


%% Choose a dataset: uncomment the desired dataset
filename = 'hw_head_1ms';
%filename = 'hw_head_3ms';
%filename = 'hw_head_30ms';

load(filename)

%% allocate space and set hyperpriors
Z = zeros(size(Y,1),size(Y,2));
W = zeros(size(Y,1),size(Y,2));

max_r = sum(h);
if length(h)>size(Y,3)
    h = h(1:size(Y,3));
else
    d = zeros(size(Y,3),1);
    d(1:length(h)) = h;
    h = d;
end
h = h/sum(h);
[~,attack] = max(h);
h = circshift(h,-attack);
reg_tv = 5;

%% per-pixel target detection

for i=1:size(Y,1)
    disp(['Processing pixel row ' num2str(i) ' of ' num2str(size(Y,1))])
    for j=1:size(Y,2)
        y = squeeze(Y(i,j,:));
        [Z(i,j),W(i,j)] = detect(y,h,max_r);
    end
end
T = (Z>0); % threshold to obtain target map

%% TV minimization
Z_tv = tv_denoising(Z, reg_tv); % minimize the perimeter of the segmentation by using TV regularization
T_tv = (Z_tv>0); % threshold to obtain TV target map

%% Plot results
figure(1)
subplot(211)
imagesc(T)
axis off
axis image
title('proposed')
subplot(212)
imagesc(T_tv)
axis off
axis image
title('proposed + TV')

%% save results
save(['results_' filename],'Z','T','Z_tv','T_tv')