function sol = tv_denoising(x, lambda)
%   Reference:  A. Beck and M. Teboulle. Fast gradient-based algorithms for constrained
%     total variation image denoising and deblurring problems. Image
%     Processing, IEEE Transactions on, 18(11):2419--2434, 2009.

max_it = 500;
tol = 1e-3;

I = x*0;
r = [I(2:end, :,:)-I(1:end-1, :,:) ; zeros(1, size(I, 2),size(I, 3))];
s = [I(:, 2:end,:)-I(:, 1:end-1,:) , zeros(size(I, 1), 1,size(I, 3))];
pold = r; qold = s;
told = 1; prev_obj = 0;

disp('Running TV denoising')

for iter = 1:max_it
    
    % divergence
    y = [r(1, :,:) ; ...
        r(2:end-1, :,:)-r(1:end-2, :,:) ;...
        -r(end-1, :,:)];
    y = y + [s(:, 1,:) ,...
        s(:, 2:end-1,:)-s(:, 1:end-2,:) ,...
        -s(:, end-1,:)];

    % Current solution
    sol = x - lambda*y;

    % Objective function value
    dx = [sol(2:end, :,:)-sol(1:end-1, :,:) ; zeros(1, size(I, 2),size(I, 3))];
    dy = [sol(:, 2:end,:)-sol(:, 1:end-1,:) , zeros(size(I, 1), 1,size(I, 3))];
    y = reshape(sum(sum(sqrt(abs(dx).^2 + abs(dy).^2),1),2),[],1);
    obj = .5*norm(x(:)-sol(:), 2)^2 + lambda * sum(y);
    rel_obj = abs(obj-prev_obj)/obj;
    prev_obj = obj;

    if rel_obj < tol
        break;
    end

    % Update divergence vectors and project
    dx = [sol(2:end, :,:)-sol(1:end-1, :,:) ; zeros(1, size(I, 2),size(I, 3))];
    dy = [sol(:, 2:end,:)-sol(:, 1:end-1,:) , zeros(size(I, 1), 1,size(I, 3))];

    
    r = r - 1/(8*lambda) * dx; 
    s = s - 1/(8*lambda) * dy;
    
    weights = max(1, sqrt(abs(r).^2+abs(s).^2));
    
    p = r./weights; 
    q = s./weights;

    % FISTA update
    t = (1+sqrt(4*told.^2))/2;
    r = p + (told-1)/t * (p - pold); pold = p;
    s = q + (told-1)/t * (q - qold); qold = q;
    told = t;
end


disp('Finished TV denoising')
