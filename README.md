## Fast object detection (MATLAB codes)

**Paper title:** 
"Fast surface detection in single-photon Lidar waveforms"

**Authors:**
J. Tachella, Y. Altmann, J.-Y. Tourneret and S. McLaughlin

**Published in:**
EUSIPCO 2019, _La Coruña, Spain_

**Link to pdf:**
(to be updated)

## How to run this demo
1. Open code in MATLAB
2. Run the script run_detection.m

If another dataset is desired, just uncomment the desired dataset in run_detection

## Trying the code with your data
Add to the MATLAB path a my_data.mat file containing:
1. A array Y (double) containing the Lidar histograms of size(Y) = [Nr, Nc, T] where Nr and Nc are the number of vertical and horizontal pixels respectively, and T is the number of histogram bins
2. A vector h (double) containing the system's IRF of size(h) = [T, 1], scaled such that sum(h) equals the number of signal photons recorded during the calibration of the IRF with a >95% reflective calibration panel.
3.  Run the script run\_detection.m, selecting the file my_data.mat

## Requirements
MATLAB (tested only in v. 2018a)
